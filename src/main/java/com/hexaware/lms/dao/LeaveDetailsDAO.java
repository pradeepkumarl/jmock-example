package com.hexaware.lms.dao;

import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;

public interface LeaveDetailsDAO {

    Employee findEmployeeByEmpId(long empId);

    LeaveDetails save(LeaveDetails leaveDetails);
}