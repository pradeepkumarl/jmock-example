package com.hexaware.lms.dao;

import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.service.LeaveDetailsServiceImpl;
import com.hexaware.lms.util.JDBCUtil;

import java.sql.*;

public class LeaveDetailsDAOImpl implements LeaveDetailsDAO {

    private final JDBCUtil jdbcUtil;

    public LeaveDetailsDAOImpl(JDBCUtil jdbcUtil){
        this.jdbcUtil = jdbcUtil;
    }

    @Override
    public Employee findEmployeeByEmpId(long empId) {

        try (Connection connection = jdbcUtil.getDBConnection("root", "welcome");
             Statement statement = connection.createStatement();
             PreparedStatement preparedStatement = connection
                     .prepareStatement("select * from employee where emp_id=?");
        ){
            preparedStatement.setLong(1,empId);

            //ResultSet resultSet = statement.executeQuery("select * from employee where emp_id=" + empId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                int employeeId = resultSet.getInt(1);
                String empName = resultSet.getString(2);
                String city = resultSet.getString(3);
                Employee employee = new Employee(employeeId, empName);
                return  employee;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public LeaveDetails save(LeaveDetails leaveDetails) {
        Connection connection = null;
        try {
            connection = jdbcUtil.getDBConnection("root", "welcome");
            Statement statement = connection.createStatement();
            statement.executeQuery("select * from employee where emp_id=");

            //select empId from table whe rer
            return leaveDetails;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
