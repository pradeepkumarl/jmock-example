package com.hexaware.lms.model;

import java.time.LocalDate;

public class Employee {

    private long empId;
    private String empName;
    private LocalDate doj;

    private int leaveBalance;

    public void setLeaveBalance(int leaveBalance) {
        this.leaveBalance = leaveBalance;
    }

    public Employee(long empId, String empName, LocalDate doj) {
        this.empId = empId;
        this.empName = empName;
        this.doj = doj;
    }
    public Employee(long empId, String empName) {
        this.empId = empId;
        this.empName = empName;
    }

    public long getEmpId() {
        return empId;
    }

    public void setEmpId(long empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public LocalDate getDoj() {
        return doj;
    }

    public void setDoj(LocalDate doj) {
        this.doj = doj;
    }

    public int  getLeaveBalance() {
        return this.leaveBalance;
    }
}