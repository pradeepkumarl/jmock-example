package com.hexaware.lms.model;

import java.time.LocalDate;
import java.util.Objects;

public class LeaveDetails {

    private long leaveId;
    private LocalDate startDate;
    private LocalDate endDate;
    private String reason;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LeaveDetails(long leaveId, String reason){
        this.leaveId = leaveId;
        this.reason = reason;
    }

    public long getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(long leaveId) {
        this.leaveId = leaveId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaveDetails that = (LeaveDetails) o;
        return leaveId == that.leaveId &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leaveId, startDate, endDate, reason);
    }

    @Override
    public String toString() {
        return "LeaveDetails{" +
                "leaveId=" + leaveId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", reason='" + reason + '\'' +
                '}';
    }
}