package com.hexaware.lms.service;

import com.hexaware.lms.model.LeaveDetails;

public interface LeaveDetailsService {

    LeaveDetails applyForLeave(long empId, LeaveDetails leaveDetails);
}