package com.hexaware.lms.service;

import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;

import java.time.Period;
import java.time.temporal.ChronoUnit;

public class LeaveDetailsServiceImpl implements LeaveDetailsService {

    private LeaveDetailsDAO leaveDetailsDAO;
    public LeaveDetailsServiceImpl(){}

    public LeaveDetailsServiceImpl(LeaveDetailsDAO leaveDetailsDAO){
        this.leaveDetailsDAO = leaveDetailsDAO;
    }
    @Override
    public LeaveDetails applyForLeave(long empId, LeaveDetails leaveDetails) {
        Employee employee = this.leaveDetailsDAO.findEmployeeByEmpId(empId);

        if(employee == null){
            throw  new IllegalArgumentException("Invalid Employee");
        } else {
            int leaveBalance = employee.getLeaveBalance();

            long days = ChronoUnit.DAYS.between(leaveDetails.getStartDate(), leaveDetails.getEndDate());

            if (leaveBalance > days){
                return this.leaveDetailsDAO.save(leaveDetails);
            } else {
                throw new IllegalArgumentException("Insufficient leave balance");
            }
        }
    }
}