package com.hexaware.lms.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtil {

    public Connection getDBConnection(String username, String password) throws SQLException {
        Connection connection = DriverManager
                .getConnection("jdbc://mysql:localhost:3306/test_db", username, password);
        return connection;
    }
}