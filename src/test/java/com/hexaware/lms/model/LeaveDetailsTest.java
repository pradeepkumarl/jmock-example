package com.hexaware.lms.model;

import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.service.LeaveDetailsService;
import org.junit.Assert;
import org.junit.Test;

public class LeaveDetailsTest {

    private LeaveDetailsDAO leaveDetailsDAO;

    private LeaveDetailsService leaveDetailsService;

    @Test
    public void testSetLeaveId(){
        LeaveDetails leaveDetails = new LeaveDetails(5000, "going home");
        leaveDetails.setLeaveId(1000);

        Assert.assertEquals(1000, leaveDetails.getLeaveId());
        Assert.assertNotEquals(5000, leaveDetails.getLeaveId());
    }

    @Test
    public void testConstructor(){
        LeaveDetails leaveDetails = new LeaveDetails(2400, "sick leave");
        Assert.assertEquals(2400, leaveDetails.getLeaveId());
        Assert.assertEquals("sick leave", leaveDetails.getReason());
    }
}

