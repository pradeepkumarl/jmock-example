package com.hexaware.lms.service;

import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;

import static org.easymock.EasyMock.*;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.*;

public class LeaveDetailsServiceTest {

    LeaveDetails leaveDetails = null;
    LeaveDetailsDAO leaveDetailsDAO;
    LeaveDetailsService leaveDetailsService;

    @Before
    public void initializeLeaveDetails(){
        leaveDetails = new LeaveDetails(12, "sick leave");
        leaveDetailsDAO = createMock(LeaveDetailsDAO.class);
        leaveDetailsService = new LeaveDetailsServiceImpl(leaveDetailsDAO);
    }

    @Test
    public void testApplyForLeaveWithInvalidEmpID() {
       expect(leaveDetailsDAO.findEmployeeByEmpId(12)).andReturn(null);
        replay(leaveDetailsDAO);
        try {
            leaveDetailsService.applyForLeave(12, leaveDetails);
            fail("Should throw an exception");
        } catch (Exception e){
            assertNotNull(e);
            assertTrue( e instanceof IllegalArgumentException);
            assertEquals(e.getMessage(), "Invalid Employee");
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testApplyForLeaveWithValidEmpID() {
        Employee employee = new Employee(12, "Vikas", LocalDate.now());
        employee.setLeaveBalance(50);
        leaveDetails.setStartDate(LocalDate.now());
        leaveDetails.setEndDate(LocalDate.of(2022, 9,15));
        expect(leaveDetailsDAO.findEmployeeByEmpId(12)).andReturn(employee);
        replay(leaveDetailsDAO);
        leaveDetailsService.applyForLeave(12, leaveDetails);
        fail("SHould throw insufficient leave balance");
    }

    @Test
    public void testApplyForLeaveWithValidEmpIDAndValidDays() {
        Employee employee = new Employee(1, "Vikas", LocalDate.now());
        employee.setLeaveBalance(50);
        leaveDetails.setStartDate(LocalDate.now());
        leaveDetails.setEndDate(LocalDate.of(2020, 3,15));
        expect(leaveDetailsDAO.findEmployeeByEmpId(12)).andReturn(employee);
        LeaveDetails leaveDetails2 = new LeaveDetails(12, "sic leave");
        leaveDetails2.setReason("Updating the status");
        leaveDetails2.setStatus("PENDING");
        expect(leaveDetailsDAO.save(leaveDetails)).andReturn(leaveDetails2);
        replay(leaveDetailsDAO);
        try {
            LeaveDetails returnValue = leaveDetailsService.applyForLeave(12, leaveDetails);
            Assert.assertNotNull(returnValue);
            assertEquals(returnValue.getStatus(), "PENDING");
        } catch (Exception e){
            fail("SHould throw insufficient leave balance");
        }
    }

}